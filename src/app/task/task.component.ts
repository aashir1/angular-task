import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FoodDataService } from '../food-data.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  keys = [];
  foodData = {};
  filteredList = [];
  result = [];
  isEmpty = false;
  isNotFound = false;
  form: FormGroup;
  constructor(private foodService: FoodDataService) {
    // http.get('assets/organized_food_data.json')
    //   .subscribe((response) => {
    //     console.log('response: ', response);
    //     this.foodData = response;
    //     this.keys = Object.keys(this.foodData)
    //   })
    foodService.getData().subscribe(response => {
      this.foodData = response;
      this.keys = Object.keys(this.foodData)
    })
  }
  ngOnInit() {
    this.form = new FormGroup({
      foodName: new FormControl('', [Validators.required]),
    });
    this.onValueChanges();
  }
  get foodName() {
    return this.form.get('foodName');
  }

  onValueChanges = () => {
    this.foodName.valueChanges.subscribe(val => {
      if (this.isEmpty) {
        this.isEmpty = false
      }
      if (this.isNotFound) {
        this.isNotFound = false
      }
      if (this.result.length) {
        this.result.length = 0
      }
    })
  }

  clearData(e) {
    // e.preventDefault();
    let foodName = this.form.get('foodName');
    foodName.setValue("");
    this.filteredList.length = 0
    console.log('foodName: ', foodName);
    return false
  }
  onScroll() {
    if (this.result.length > this.filteredList.length) {
      this.filteredList = [...this.filteredList, ...this.result.slice(this.filteredList.length - 1, this.filteredList.length + 5)]
    }
  }
  onSubmit() {
    let { foodName } = this.form.value;
    let regex = new RegExp(foodName, "i");
    this.filteredList = []
    if (!foodName) {
      this.isEmpty = true
      return false
    }
    if (foodName) {
      console.time();
      for (let i in this.keys) {

        let test = this.keys[i].split("portion")[0].match(regex)
        if (test) {
          this.result.push(this.foodData[this.keys[i]]);
        }
      }
      if (!this.result.length) {
        this.isNotFound = true
        return false
      }
      if (this.result.length) {
        this.filteredList = this.result.slice(0, 25)
      }
      console.timeEnd();
    }
  }

}
